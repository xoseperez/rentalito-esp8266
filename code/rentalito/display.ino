/*

DISPLAY MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include "ht1632c.h"

ht1632c _matrix = ht1632c(MATRIX_DATA_PIN, MATRIX_WR_PIN, MATRIX_CLK_PIN, MATRIX_CS_PIN, 2);

// -----------------------------------------------------------------------------
// DISPLAY
// -----------------------------------------------------------------------------

void displayText(uint8_t x, uint8_t y, char * text, uint8_t color, uint8_t align = ALIGN_LEFT | ALIGN_TOP, uint8_t bufferID = 0) {
    DEBUG_MSG_P(PSTR("[DISPLAY] %s\n"), text);
    byte width = strlen(text) * _matrix.getFontWidth();
    if (width > _matrix.getDisplayWidth()) {
        _matrix.hScroll(y, text, color, SCROLL_LEFT, 20, bufferID);
    } else {
        _matrix.putText(x, y, text, color, align);
    }
}

void displayShow(display_mode_t mode, char * row1, char * row2) {

    _matrix.stopScrolling();
    _matrix.clear();

    if (mode == DISPLAY_MESSAGE) {
        _matrix.setFont(FONT_8x13B);
        displayText(0, 2, row1, ORANGE, ALIGN_CENTER);
    }

    if (mode == DISPLAY_MESSAGE_2LINES) {
        _matrix.setFont(FONT_5x7);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        displayText(0, 9, row2, RED, ALIGN_CENTER, 1);
    }

    if (mode == DISPLAY_KEY) {
        _matrix.setFont(FONT_8x8);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        _matrix.setFont(FONT_5x7);
        displayText(0, 9, row2, GREEN, ALIGN_CENTER, 1);
    }

    if (mode == DISPLAY_VALUE) {
        _matrix.setFont(FONT_5x7);
        displayText(0, 1, row1, RED, ALIGN_CENTER, 0);
        _matrix.setFont(FONT_8x8);
        displayText(0, 8, row2, GREEN, ALIGN_CENTER, 1);
    }

    _matrix.sendframe();

}

void displayShow(display_mode_t mode, char * row1) {
    displayShow(mode, row1, "");
}

void displayClear() {
    _matrix.stopScrolling();
    _matrix.clear();
    _matrix.sendframe();
    //DEBUG_MSG("[DISPLAY] Clear display");
}

void displaySetBrightness() {
    uint8_t brightness = getSetting("brightness", String(MATRIX_BRIGHTNESS)).toInt();
    _matrix.setBrightness(brightness);
    DEBUG_MSG_P(PSTR("[DISPLAY] Brightness: %d\n"),brightness);
}

bool displayScrolling() {
    return _matrix.scrolling();
}

void showWelcome() {
    displayShow(DISPLAY_MESSAGE_2LINES, (char *) APP_NAME, (char *) APP_VERSION);
    channelsResetUpdate();
}

void showWaiting() {
    displayShow(DISPLAY_MESSAGE_2LINES, (char *) "WAITING...");
    channelsResetUpdate();
}

void clearScreen() {
    displayClear();
    channelsResetUpdate();
}

void displaySetup() {
    _matrix.begin();
    _matrix.setBrightness(MATRIX_BRIGHTNESS);
    displaySetBrightness();
}

void displayLoop() {

    _matrix.scroll();

    static unsigned long last_tick = 0;
    static bool tick = false;
    if (channelsMode() == 0) {
        if (millis() - last_tick > MATRIX_TICK_INTERVAL) {
            last_tick = millis();
            tick = !tick;
            _matrix.setPixel(63, 15, tick ? GREEN : BLACK);
            _matrix.sendframe();
        }
    }

}
