/*

CHANNELS MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#include <vector>

// -----------------------------------------------------------------------------
// GLOBALS
// -----------------------------------------------------------------------------

struct channel_t {
    char * value;
    unsigned long last;
};

std::vector<channel_t> _channels;
unsigned char _channels_current = 0;
unsigned long _channels_last_update = 0;
unsigned char _channels_mode = CHANNELS_MODE_DEFAULT;

// -----------------------------------------------------------------------------
// Private methods
// -----------------------------------------------------------------------------

void _channelsReset() {
    DEBUG_MSG_P(PSTR("[CHANNELS] Resetting channels\n"));
    for (byte i=0; i< _channels.size(); i++) {
        free(_channels[i].value);
    }
    _channels.clear();
    for (byte i=0; i<CHANNELS_MAX_CHANNELS; i++) {
        _channels.push_back((channel_t) {NULL, 0});
    }
}

bool _channelsShow(unsigned char index) {

    // No message arrived to this channel yet
    if (_channels[index].last == 0) return false;

    // Check expiration
    unsigned long expire = 1000 * getSetting("expire", index, CHANNELS_DEFAULT_EXPIRE).toInt();
    if ((millis() - _channels[index].last) > expire) return false;

    // Check filter
    String filter = getSetting("filter", index, "");
    if ((filter.length()>0) && (!filter.equals(_channels[index].value))) return false;

    // Get the name and value templates and replace the actual value
    String name = getSetting("name", index, "");
    name.replace("{value}", _channels[index].value);
    String value = getSetting("value", index, "");
    value.replace("{value}", _channels[index].value);

    if (value.length() == 0) {
        displayShow(DISPLAY_MESSAGE, (char *) name.c_str());
    } else {
        displayShow(DISPLAY_VALUE, (char *) name.c_str(), (char *) value.c_str());
    }

    channelsResetUpdate();

    return true;

}

void _channelsShowNext() {

    unsigned char index = _channels_current;
    while (true) {
        index = (index + 1) % CHANNELS_MAX_CHANNELS;
        if (_channelsShow(index)) break;
        if (index == _channels_current) {
            showWaiting();
            break;
        }
    }
    _channels_current = index;
}

void _channelsMatch(unsigned int type, const char * topic, const char * payload) {

    if (type == MQTT_CONNECT_EVENT) {
        mqttSubscribeRaw("#");
    }

    if (type == MQTT_MESSAGE_EVENT) {

        // Builtin topics
        String t = mqttSubtopic((char *) topic);
        if (t.equals(MQTT_TOPIC_MODE)) {
            channelsMode(atoi(payload));
            return;
        }
        if (t.equals(MQTT_TOPIC_BRIGHTNESS)) {
            int brightness = atoi(payload) & 0x0F;
            setSetting("brightness", brightness);
            displaySetBrightness();
            return;
        }

        // Find topic
        byte topics = getSetting("topicCount", 0).toInt();
        for (byte i=0; i<topics; i++) {
            if (getSetting("topic", i, "").equals(topic)) {

                // Match! Blink in happyness
                ledOn();

                // Clear previous value
                if (_channels[i].value) free(_channels[i].value);

                // Store values
                _channels[i].last = millis();
                _channels[i].value = strdup(payload);
                DEBUG_MSG("[CHANNELS] Matched %s => %s\n", topic, _channels[i].value);

                // Check prioritary
                if (getSetting("prioritary", i, 0).toInt() == 1) {
                    _channelsShow(i);
                }

                ledOff();

                break;

            }
        }

    }

}

// -----------------------------------------------------------------------------
// Public API
// -----------------------------------------------------------------------------

void channelsResetUpdate() {
    _channels_last_update = millis();
}

void channelsMode(unsigned char mode) {
    _channels_mode = mode & 0x01;
    setSetting("mode", _channels_mode);
    saveSettings();
    DEBUG_MSG_P(PSTR("[CHANNELS] Setting mode to %d\n"), _channels_mode);
}

unsigned char channelsMode() {
    return _channels_mode;
}

void channelsConfigure() {
    _channels_mode = getSetting("mode", CHANNELS_MODE_DEFAULT).toInt();
    _channelsReset();
}

// -----------------------------------------------------------------------------

void channelsSetup() {
    channelsConfigure();
    mqttRegister(_channelsMatch);
}

void channelsLoop() {
    if ((millis() - _channels_last_update) > CHANNELS_UPDATE_INTERVAL) {
        if (displayScrolling()) {
            _channels_last_update += 1000;
        } else if (_channels_mode == CHANNELS_MODE_NORMAL) {
            _channelsShowNext();
        } else {
            clearScreen();
        }
    }
}
