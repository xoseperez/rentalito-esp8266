/*

ALEXA MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#if ALEXA_SUPPORT

#include <fauxmoESP.h>

fauxmoESP alexa;

// -----------------------------------------------------------------------------
// ALEXA
// -----------------------------------------------------------------------------

void alexaConfigure() {
    alexa.enable(getSetting("alexaEnabled", ALEXA_ENABLED).toInt() == 1);
}

void alexaSetup() {

    // Backwards compatibility
    moveSetting("fauxmoEnabled", "alexaEnabled");

    alexaConfigure();
    alexa.addDevice("rentalito");
    alexa.onSetState([](unsigned char device_id, const char * name, bool state) {
        channelsMode(state);
    });
    alexa.onGetState([](unsigned char device_id, const char * name) {
        return channelsMode() ? 1 : 0;
    });

}

void alexaLoop() {
    alexa.handle();
}

#endif
