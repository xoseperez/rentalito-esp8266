#include <Arduino.h>
#include <EEPROM.h>
#include <NtpClientLib.h>
#include <ESPAsyncWebServer.h>
#include <AsyncMqttClient.h>
#include <functional>

typedef enum {
    DISPLAY_MESSAGE,
    DISPLAY_MESSAGE_2LINES,
    DISPLAY_KEY,
    DISPLAY_VALUE,
} display_mode_t;


void mqttRegister(void (*callback)(unsigned int, const char *, const char *));
String mqttSubtopic(char * topic);

template<typename T> bool setSetting(const String& key, T value);
template<typename T> bool setSetting(const String& key, unsigned int index, T value);
template<typename T> String getSetting(const String& key, T defaultValue);
template<typename T> String getSetting(const String& key, unsigned int index, T defaultValue);

char * ltrim(char * s);
