//-----------------------------------
// CONFIGURATION
//-----------------------------------

width = 241.30;
height = 147.32;
chamfer = 5;
hole_size = 1.5;

l2 = 226.06;
l4 = 132.08;
lX = 278;

x1 = (width - l2) / 2;
x2 = x1 + l2;
x3 = x2 + 2 * x1;
x4 = x3 + l2;

y1 = (height - l4) / 2;
y2 = y1 + l4;

holes = [
    [x1,y1],
    [x2,y1],
    [x3,y1],
    [x4,y1],
    [x1,y2],
    [x2,y2],
    [x3,y2],
    [x4,y2]
];

//-----------------------------------


module base() {
    difference() {
        
        // base
        minkowski() {
            square([width*2, height]);
            circle(chamfer);
        }
        
        // holes
        for (hole = holes)
            translate(hole) 
                circle(hole_size, $fn=50);
        
    }
}

module pcb() {
    holes = [[3,3],[47,3],[47,47],[3,47]];
    for (hole = holes)
        translate(hole) 
            circle(hole_size, $fn=50);
}

module wall1() {
    union() {
        circle(hole_size+2, $fn=50);
        translate([0,5]) 
            minkowski() {
                square([0.1,10], true);
                circle(2, $fn=50);
            }
    }
}

module wall() {
    wall1();
    translate([lX,0]) wall1();
}

module back() {
    difference() {
        base();
        translate([22,22]) pcb();
        translate([(width*2 - lX)/2,y2-10]) wall();
    }
}

module front() {
    base();
}

translate([chamfer, chamfer]) {
    back();
    translate([0,height + 2*chamfer+ 5]) front();
}

